package main.hcl.com;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    Calculator cal=new Calculator();
    @Test
    public void add() {
        assertEquals(4,cal.add(2,2));
    }

    @Test
    public void sub() {
        assertEquals(2,cal.sub(4,2));
    }

    @Test
    public void mul() {
        assertEquals(9,cal.mul(3,3));
    }

    @Test
    public void divide() {
        assertEquals(2.0,cal.divide(10,5));
    }
}